<?php

namespace App\Models;

use CodeIgniter\Model;

class M_photo extends Model
{
    protected $table = 'photo';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    public function getAllByIdCompet($prmIdCompet)
    {
        $requete = $this->select('photo.ID, Titre, Prenom, Nom, Pays, Classement')
            ->join('concurrent', 'photo.concurrentID = concurrent.ID', 'left')
            ->where(['photo.competitionID' => $prmIdCompet])
            ->orderBy('Classement', 'ASC');
            return $requete->findAll();
    }

    public function getDetail($prmId)
    {
        $requete = $this->where(['ID' => $prmId]);
        return $requete->findAll();
    }
}
