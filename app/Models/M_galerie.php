<?php

namespace App\Models;

use CodeIgniter\Model;

class M_galerie extends Model
{
    protected $table = 'photo';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    //fonction pour tout afficher
    public function getAll()
    {
        $requete = $this->select('ID, Titre, NomFichier');
        return $requete->findAll();
    }

    public function show($prmId=null)
    {
        $result=$this->model->getDetail($prmId);
        if (count($result)!=0){
            return $this->respond($result[0],200);
        }else{
            return $this->respond(null, $this->codes['invalid_request']);
        }
    }

    public function getDetail($prmId)
    {
        $requete = $this->select('*')
            ->where(['ID' => $prmId]);
        return $requete->findAll();
    }

}
