<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('css/styles.css'); ?>">
    <title>Concours photographique des Dév. Web</title>
</head>

<body>
    <div id="conteneur">
        <header><h1><?php echo $page_title; ?></h1></header>
        <nav>
            <ul>
                <?php
                $ctrl = current_url(true)->getSegment(1);
                if ($ctrl == "") $ctrl = "C_accueil";
                ?>
                <li><a href=" <?php echo base_url("C_accueil"); ?>">Accueil</a></li>
                <li><a href=" <?php echo base_url("C_competition"); ?>">Les compétitions</a></li>
                <li><a href=" <?php echo base_url("C_galerie"); ?>">Galerie photos</a></li>
            </ul>
        </nav>
        <section>
            <?php echo $contenu; ?>
        </section>
        <footer></footer>
    </div>
</body>

</html>