<?php

namespace App\Controllers;
use CodeIgniter\Controller;


class C_accueil extends BaseController
{
	public function index()
	{
		$data['page_title'] = "Concours photographique";
        $data['titre1'] = "Les résultats du concours sont publiés";

        $page['contenu'] = view('V_accueil', $data);

        return view('Commun/v_template', $page);
	}
}