<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_galerie;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_galerie extends Controller
{
    public function index()
    {
        $model = new M_galerie();
        $data['result'] = $model->getAll();

        $data['page_title'] = "Concours photographique";
        $data['titre1'] = "Galerie photos";
        $data['pager'] = $model->pager;

        $page['contenu'] =
            view('V_galerie', $data);
        return view('Commun/v_template', $page);
    }

    public function detail($prmId = null)
    {
            $model = new M_galerie();
            
            $data['result'] = $model->getDetail($prmId);
            $data['page_title'] = "Détail d'une photo";
            $data['titre1'] = "La photo " . $prmId;
            $page['contenu'] = view('competition/V_detail_photo', $data);
            return view('Commun/v_template', $page);
    }
}