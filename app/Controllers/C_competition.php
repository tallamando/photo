<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_competition;
use App\Models\M_photo;
use CodeIgniter\Exceptions\PageNotFoundException;

class C_competition extends Controller
{
    public function index()
    {
        $model = new M_competition();
        $data['result'] = $model->getAll();

        $data['page_title'] = "Concours photographique";
        $data['titre1'] = "La liste des compétitions";
        $data['pager'] = $model->pager;

        $page['contenu'] =
            view('competition/V_liste_competition', $data);
        return view('Commun/v_template', $page);
    }

    public function detail($prmId = null)
    {
        if ($prmId !=null){
            $model = new M_competition();
            $modelPhoto = new M_photo();
            $data['resCompet'] = $model->getDetail($prmId);
            if (count($data['resCompet'])!=0){
                $data['page_title'] = "Détail d'une compétition";
                $data['titre1'] = "La compétition " . $data['resCompet'][0]['Nom'];
                $data['resPhotos'] = $modelPhoto->getAllByIdCompet($prmId);
                $page['contenu'] = view('competition/V_detail_competition', $data);
                return view('Commun/v_template', $page);
        }else{
            throw PageNotFoundException::forPageNotFound("Cette compétition n'existe pas!");
        }
    }else{
        throw PageNotFoundException::forPageNotFound("Il faut choisir une compétition!");
    }
  }
}